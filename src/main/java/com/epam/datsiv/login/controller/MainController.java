package com.epam.datsiv.login.controller;

import com.epam.datsiv.login.domain.Role;
import com.epam.datsiv.login.domain.User;
import com.epam.datsiv.login.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    UserRepo userRepo;

    @GetMapping("/main")
    public String main() {
        return "main";
    }

    @GetMapping("/")
    public String greeting() {
        return "greeting";
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Map<String, Object> model) {
        User userFromDB = userRepo.findByUsername(user.getUsername());

        if(userFromDB != null) {
            model.put("message", "user exists");
            return "registration";
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        userRepo.save(user);

        return "redirect:/login";
    }


}
package com.epam.datsiv.login.repos;


import com.epam.datsiv.login.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
